package com.afs.restapi;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTest {
    @Autowired
    MockMvc postman;

    ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private EmployeeRepository employeeRepository;

    @BeforeEach
    void setup() {
        employeeRepository.reset();
    }

    @Test
    void should_return_all_employees_when_get_all_given_employees() throws Exception {
        //given
        //when
        postman.perform(MockMvcRequestBuilders.get("/employees"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(5)));
        //then
    }

    @Test
    void should_return_employee_when_get_a_employee_given_employees() throws Exception {
        //given
        Employee employee = new Employee(1L, "John Smith", 32, "Male", 5000);
        String expected = objectMapper.writeValueAsString(employee);
        //when
        postman.perform(MockMvcRequestBuilders.get("/employees/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(mvcResult -> {
                    String json = mvcResult.getResponse().getContentAsString();
                    assertEquals(expected, json);
                });
        //then
    }

    @Test
    void should_have_first_employee_status_false_when_delete_a_employee_given_employees() throws Exception {
        //given

        //when
        postman.perform(MockMvcRequestBuilders.delete("/employees/1"))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
        //then
        postman.perform(MockMvcRequestBuilders.get("/employees"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].activeStatus").value(false));
    }
    
    @Test
    void should_return_male_employees_when_get_employees_by_gender_given_employee_gender() throws Exception {
        //given
        MockHttpServletRequestBuilder mockMvcRequestBuilders = MockMvcRequestBuilders.get("/employees")
                .param("gender", "Male");
        //when
        postman.perform(mockMvcRequestBuilders)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$..gender").value(everyItem(is("Male"))));
        //then
    }

    @Test
    void should_return_id_three_and_four_when_get_by_page_number_and_page_size_given_page_number_two_and_page_size_two() throws Exception {
        //given
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee(3L, "David Williams", 35,"Male", 5500));
        employees.add(new Employee(4L, "Emily Brown", 23, "Female", 4500));
        String expected = objectMapper.writeValueAsString(employees);
        MockHttpServletRequestBuilder mockMvcRequestBuilders = MockMvcRequestBuilders.get("/employees")
                .param("pageNumber", "2")
                .param("pageSize", "2");
        //when
        postman.perform(mockMvcRequestBuilders)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(mvcResult -> {
                    assertEquals(expected, mvcResult.getResponse().getContentAsString());
                });
        //then
    }
    
    @Test
    void should_return_new_employee_when_create_employee_given_employee() throws Exception {
        //given
        Employee employee = new Employee("Ivan", 18,"Male", 5500);
        String bodyString = objectMapper.writeValueAsString(employee);
        Employee expectedEmployee = new Employee(6L, "Ivan", 18,"Male", 5500);
        String expected = objectMapper.writeValueAsString(expectedEmployee);
        MockHttpServletRequestBuilder mockMvcRequestBuilders = MockMvcRequestBuilders.post("/employees")
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyString);

        //when
        postman.perform(mockMvcRequestBuilders)
                .andExpect(MockMvcResultMatchers.status().isCreated());

        postman.perform(MockMvcRequestBuilders.get("/employees/6"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(result -> assertEquals(expected, result.getResponse().getContentAsString()));
        //then
    }

    @Test
    void should_return_updated_employee_when_update_employee_given_age_and_salary() throws Exception {
        //given
        Employee employee = new Employee(null, 18, null, 5500);
        String bodyString = objectMapper.writeValueAsString(employee);
        Employee expectedEmployee = new Employee(2L, "Jane Johnson", 18, "Female", 5500);
        String expected = objectMapper.writeValueAsString(expectedEmployee);
        MockHttpServletRequestBuilder mockMvcRequestBuilder = MockMvcRequestBuilders.put("/employees/2")
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyString);
        //when
        postman.perform(mockMvcRequestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk());
        //then
        postman.perform(MockMvcRequestBuilders.get("/employees/2"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(result -> assertEquals(expected, result.getResponse().getContentAsString()));
    }

    @Test
    void should_return_404_when_get_employee_given_not_existed_employee() throws Exception {
        //given
        //when
        postman.perform(MockMvcRequestBuilders.get("/employees/999"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
        //then
    }

    @Test
    void should_return_404_when_update_employee_given_not_existed_employee() throws Exception {
        //given
        MockHttpServletRequestBuilder mockMvcRequestBuilder = MockMvcRequestBuilders.put("/employees/999")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{}");
        //when
        postman.perform(mockMvcRequestBuilder)
                .andExpect(MockMvcResultMatchers.status().isNotFound());
        //then
    }

    @Test
    void should_return_404_when_delete_employee_given_not_existed_employee() throws Exception {
        //given
        //when
        postman.perform(MockMvcRequestBuilders.delete("/employees/999"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
        //then
    }
}
