package com.afs.restapi;

import com.afs.restapi.exception.InvalidEmployeeException;
import com.afs.restapi.exception.NotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class EmployeeServiceTest {
    @Mock
    EmployeeRepository employeeRepository;

    @InjectMocks
    EmployeeService employeeService;

    @Test
    void should_throw_invalid_employee_exception_when_create_employee_given_employee_age_less_than_18() {
        //given
        Employee newEmployee = new Employee("Ivan", 16, "Male", 1000);
        EmployeeService employeeService = new EmployeeService();
        //when
        Throwable exception = assertThrows(InvalidEmployeeException.class, () -> employeeService.create(newEmployee));
        assertEquals("Employee must be 18~65 years old", exception.getMessage());
        //then
    }

    @Test
    void should_throw_invalid_employee_exception_when_create_employee_given_employee_age_larger_than_65() {
        //given
        Employee newEmployee = new Employee("Ivan", 66, "Male", 1000);
        EmployeeService employeeService = new EmployeeService();
        //when
        Throwable exception = assertThrows(InvalidEmployeeException.class, () -> employeeService.create(newEmployee));
        assertEquals("Employee must be 18~65 years old", exception.getMessage());
        //then
    }

    @Test
    void should_add_new_employee_when_create_employee_given_employee_age_between_18_and_65() {
        //given
        Employee newEmployee = new Employee("Ivan", 45, "Male", 1000);
        //when
        employeeService.create(newEmployee);
        //then
        verify(employeeRepository, times(1)).insert(newEmployee);
    }

    @Test
    void should_return_all_employees_when_get_all_given_employees() {
        //given
        //when
        employeeService.getAllEmployees();
        //then
        verify(employeeRepository, times(1)).findAll();
    }
    
    @Test
    void should_return_employee_when_get_employee_by_id_given_employee_id() {
        //given
        //when
        employeeService.getEmployeeById(1L);
        //then
        verify(employeeRepository, times(1)).findById(1L);
    }

    @Test
    void should_return_male_employees_when_get_employee_by_gender_given_gender_is_male() {
        //given
        String gender = "Male";
        //when
        employeeService.getEmployeeByGender(gender);
        //then
        verify(employeeRepository, times(1)).findByGender(gender);
    }

    @Test
    void should_return_employees_when_get_employee_by_page_number_and_page_size_given_page_number_and_page_size() {
        //given
        Integer pageNumber = 2;
        Integer pageSize = 3;
        //when
        employeeService.getEmployeeByPage(pageNumber, pageSize);
        //then
        verify(employeeRepository, times(1)).findByPage(pageNumber, pageSize);
    }

    @Test
    void should_return_employee_when_update_employee_given_salary_and_age() {
        //given
        Long id = 1L;
        Integer age = 20;
        Integer salary = 1000;
        Employee newEmployee = new Employee(null, age, null, salary);
        //when
        employeeService.updateEmployee(id, newEmployee);
        //then
        verify(employeeRepository, times(1)).update(id, newEmployee);
    }

    @Test
    void should_make_employee_inactive_when_delete_employee_given_employee_exists() {
        //given
        Long id = 1L;
        //when
        employeeService.deleteEmployee(id);
        //then
        verify(employeeRepository, times(1)).delete(id);
    }

    @Test
    void should_return_not_found_exception_when_get_employee_by_id_given_employee_does_not_exists() {
        //given
        EmployeeRepository employeeRepository = new EmployeeRepository();
        EmployeeService employeeService = new EmployeeService(employeeRepository);
        //when
        Throwable exception = assertThrows(NotFoundException.class, () -> employeeService.getEmployeeById(999L));
        assertEquals("Employee is inactive", exception.getMessage());
        //then
    }

    @Test
    void should_return_not_found_exception_when_update_employee_by_id_given_employee_does_not_exists() {
        //given
        EmployeeRepository employeeRepository = new EmployeeRepository();
        EmployeeService employeeService = new EmployeeService(employeeRepository);
        //when
        Throwable exception = assertThrows(NotFoundException.class, () -> employeeService.updateEmployee(999L, new Employee()));
        assertEquals("Employee is inactive", exception.getMessage());
        //then
    }

    @Test
    void should_return_not_found_exception_when_delete_employee_by_id_given_employee_does_not_exists() {
        //given
        EmployeeRepository employeeRepository = new EmployeeRepository();
        EmployeeService employeeService = new EmployeeService(employeeRepository);
        //when
        Throwable exception = assertThrows(NotFoundException.class, () -> employeeService.deleteEmployee(999L));
        assertEquals("Employee is inactive", exception.getMessage());
        //then
    }

    @Test
    void should_have_true_active_status_when_create_employee_given_employee() {
        //given
        Employee newEmployee = new Employee("Ivan", 45, "Male", 1000);
        EmployeeRepository employeeRepository = new EmployeeRepository();
        EmployeeService employeeService = new EmployeeService(employeeRepository);
        //when
        employeeService.create(newEmployee);
        Employee employee = employeeRepository.findById(6L);
        //then
        assertEquals(true, employee.getActiveStatus());
    }

    @Test
    void should_have_true_active_status_when_remove_employee_given_employee() {
        //given
        EmployeeRepository employeeRepository = new EmployeeRepository();
        EmployeeService employeeService = new EmployeeService(employeeRepository);
        //when
        Employee employee = employeeRepository.findById(4L);
        employeeService.deleteEmployee(4L);
        //then
        assertEquals(false, employee.getActiveStatus());
    }
}
