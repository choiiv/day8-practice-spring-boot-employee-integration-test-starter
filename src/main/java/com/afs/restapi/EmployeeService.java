package com.afs.restapi;

import com.afs.restapi.exception.InvalidEmployeeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    @Autowired
    EmployeeRepository employeeRepository;

    public EmployeeService() {

    }

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public Employee create(Employee employee) {
        if(employee.getAge() < 18 || employee.getAge() > 65) {
            throw new InvalidEmployeeException();
        }
        return employeeRepository.insert(employee);
    }

    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    public Employee getEmployeeById(Long id) {
        return employeeRepository.findById(id);
    }

    public List<Employee> getEmployeeByGender(String gender) {
        return employeeRepository.findByGender(gender);
    }

    public List<Employee> getEmployeeByPage(Integer pageNumber, Integer pageSize) {
        return employeeRepository.findByPage(pageNumber, pageSize);
    }

    public Employee updateEmployee(Long id, Employee employee) {
        return employeeRepository.update(id, employee);
    }

    public void deleteEmployee(Long id) {
        employeeRepository.delete(id);
    }
}
